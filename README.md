# MLIP-iron

Machine-learning interatomic potential for radiation damage effects in bcc-iron

# Description of the Sub-potentials

This machine-learning potential consists of three sub-potentials, 1. MTP-L (Fe.long.mtp), 2. MTP-S (Fe.short.mtp) 3. ZBL (Fe.zbl.table)

The first two sub-potentials are machine-learning moment tensor potentials. And the last is the universal universal Ziegler-Biersack-Littmarck function with the default parameters (for iron).

All three sub-potentials could be individually used. However, they are only accurate for limited local environments.

Specifically, MTP-L could be used for simulations in which atomic distances are always longer than 1.9 Angstrom. 

MTP-S could be used for simulations in which atomic distances are always less than 1.9 Angstrom, but also longer than 1.1 Angstrom.

The ZBL potential is pairwise, and hence should only be used in proper simulations.

# Before Usage
The potential files are intended to be used with the LAMMPS software.

Before usage, the LAMMPS should patched and re-compiled.

Two modifications should be made to the LAMMPS source.

1). The pair_table_mod.cpp and pair_table_mod.h file in directory "pair_table_mod source" should be copied into the "src" folder of LAMMPS source, and then re-compile the LAMMPS.

Note that different pair_table_mod.cpp should be used according the version of LAMMPS on your own machine.

2). The moment tensor potential package (MLIP-2，https://gitlab.com/ashapeev/mlip-2) and its LAMMPS interface developed by Dr. Alexander Shapeev's team should be compiled into the LAMMPS source.

Please note that the MLIP-2 is a copyright code, access to the code should be consulted with its developers.

(see https://mlip.skoltech.ru/)


Additionally, to use the hybridazed moment tensor potentials, one should seek for the "yiwang" branch within the MLIP-2 repository.

And one should pay attention to the "INSTALL.md" file of the "yiwang" branch within the MLIP-2, on the section "Additional patch for usage of the hybridization of moment tensor potentials"


# Usage
A. Individual usage of one sub-potential:

When individually used, the MTP-L and MTP-S potentials can be treated as any normal moment tensor potentials. A moment tensor potential would require a *.ini file to provide information on the path of the potential file. Examples are given as Fe.long.ini and Fe.short.ini .

The ZBL potential is not expected to be individually used because it cannot practially describe the properties of bcc iron.

But if in case tests are need, we suggest to use it with the following LAMMPS scripts:


pair_style  table/mod spline 1000000

pair_coeff * * Fe.zbl.table FeFe 2.0



B. Usage of the full potential with all three sub-potentials:

We suggest users always use the full potential with all three sub-potentials. This is achieved using the "hybrid/overlay" function of LAMMPS, so the full potential is activated with proper accuracy.

One could take the "pot.mod" file as an example to fully use the potential.

The simplest way is using "include pot.mod" in LAMMPS scripts.

(Please remember to modifiy the file paths)

